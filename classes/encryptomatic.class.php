<?php

class Encryptomatic {
	
	protected $input 	= '';
	protected $method	= '';
	protected $key_hash	= KEY_HASH;
	
	public $the_hash = '';
	
	public function __construct( $string, $meth )
	{	
		$this->input 	= $string;
		$this->method	= $meth;
		
		if( $this->method === 'encrypt' ){
			$this->encrypt();
		}elseif( $this->method === 'decrypt' ){
			$this->decrypt();
		}
	}
	
	protected function encrypt()
	{
		$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($this->key_hash), $this->input, MCRYPT_MODE_CBC, md5(md5($this->key_hash))));
		$this->the_hash = $encrypted;
		// return $encrypted;
	}
	
	protected function decrypt()
	{
		$decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($this->key_hash), base64_decode($this->input), MCRYPT_MODE_CBC, md5(md5($this->key_hash))), "\0");
		$this->the_hash = $decrypted;
		// return $decrypted;
	}
	
	public function get_hash()
	{
		return $this->the_hash;
	}
	
}
<?php
require_once 'encryptomatic.class.php';

class enrolled_users_list_table extends WP_List_Table {

   /**
    * Constructor, we override the parent to pass our own arguments
    * We usually focus on three parameters: singular and plural labels, as well as whether the class supports AJAX.
    */
	function __construct() {
		
		parent::__construct( 
			
			array(
				'singular'		=> 'wp_list_text_link', //Singular label
				'plural'		=> 'wp_list_test_links', //plural label, also this well be one of the table css class
				'ajax'			=> false //We won't support Ajax for this table
			) 	
		);
	}
	
	/**
	 * Add extra markup in the toolbars before or after the list
	 * @param string $which, helps you decide if you add the markup after (bottom) or before (top) the list
	 */
	function extra_tablenav( $which ) {
	   if ( $which == "top" ){
	      //The code that goes before the table is here
	      // echo plugin_dir_url(dirname(__FILE__));
	   }
	   if ( $which == "bottom" ){
	      //The code that goes after the table is there
	      // echo"Hi, I'm after the table";
	   }
	}
	
	/**
	 * Define the columns that are going to be used in the table
	 * @return array $columns, the array of columns to use with the table
	 */
	function get_columns()
	{
		return $columns = array(
			// 'col_sID'						=>__('ID'),
			'col_customer_number'			=>__('Customer #'),
			'col_user_key'					=>__('User Key'),
			'col_last_name'					=>__('Last Name'),
			'col_first_name'				=>__('First Name'),
			'col_email'						=>__('Email'),
			'col_enrolled'					=>__('Pebble Program?'),
			'col_program_progress'			=>__('Program Progress'),
			'col_staff'						=>__('Staff Member?'),
		);
	}
	
	/**
	 * Decide which columns to activate the sorting functionality on
	 * @return array $sortable, the array of columns that can be sorted by the user
	 */
	public function get_sortable_columns()
	{
		return $sortable = array(
			'col_customer_number'			=> array('Customer No', true),
			'col_last_name'					=> array('Last Name', false),
		);
	}
	
	function column_col_customer_number( $item )
	{
		$actions = array(
			'edit' 		=> sprintf( '<a href="?page=%s&action=%s&uid=%s">Edit</a>','srixon-enrolled-users','edit',$item->id ),
			'delete' 	=> sprintf( '<a href="?page=%s&action=%s&uid=%s">Delete</a>','srixon-enrolled-users','delete',$item->id ),
		);
 
		 return $item->{'CUSTOMER #'}."<br>".$this->row_actions($actions);
	}
	
	function column_col_sID( $item )
	{
		$actions = array(
			'edit' 		=> sprintf( '<a href="?page=%s&action=%s&uid=%s">Edit</a>',$_REQUEST['page'],'edit',$item->id ),
			'delete' 	=> sprintf( '<a href="?page=%s&action=%s&uid=%s">Delete</a>',$_REQUEST['page'],'delete',$item->id ),
		);
 
		 return $item->id."<br>".$this->row_actions($actions);
	}
	
	function column_col_user_key( $item )
	{
		$new_en		= new Encryptomatic( $item->{'USER KEY'}, 'decrypt' );
		$decrypto	= $new_en->get_hash();
		return $decrypto;
	}
	
	function column_col_last_name( $item )
	{
		
		return $item->LAST;
	}
	
	function column_col_first_name( $item )
	{
		return $item->FIRST;
	}
	
	function column_col_email( $item )
	{
		return $item->EMAIL;
	}
	
	function column_col_enrolled( $item )
	{
		$val	= $item->{'PEBBLE BEACH PROGRAM'} == 1 ? 'Yes' : 'No';
		return $val;
	}
	
	function column_col_program_progress( $item )
	{
		setlocale(LC_MONETARY, 'en_US');
		$money		= money_format('%.0n', floatval($item->{'PEBBLE BEACH SALES'}));
		return $money;
	}
	
	function column_col_staff( $item )
	{
		$val	= $item->{'STAFF MEMBER'} == 1 ? 'Yes' : 'No';
		return $val;
	}
	
	/**
	 * Prepare the table with different parameters, pagination, columns and table elements
	 */
	function prepare_items() {
		global $wpdb, $_wp_column_headers;
		$screen = get_current_screen();

		/* -- Preparing your query -- */
		$query = "SELECT * FROM ".SRIXON_USER_TABLE;

		/* -- Ordering parameters -- */
		//Parameters that are going to be used to order the result
		$orderby		= !empty($_GET["orderby"]) ? mysql_real_escape_string($_GET["orderby"]) : 'CustomerNo';
		if( $orderby == 'LastName' ){
			$orderby = "`LAST`";
		}elseif( $orderby == "CustomerNo" ){
			$orderby = "`CUSTOMER #`";
		}
		
		$order			= !empty($_GET["order"]) ? mysql_real_escape_string($_GET["order"]) : 'ASC';
		if(!empty($orderby) & !empty($order)){ $query.=' ORDER BY '.$orderby.' '.$order; }

		/* -- Pagination parameters -- */
		//Number of elements in your table?
		$totalitems 	= $wpdb->query($query); //return the total number of affected rows
		//How many to display per page?
		$perpage 		= 20;
		//Which page is this?
		$paged 			= !empty($_GET["paged"]) ? mysql_real_escape_string($_GET["paged"]) : '';
		//Page Number
		if(empty($paged) || !is_numeric($paged) || $paged<=0 ){ $paged=1; }
		//How many pages do we have in total?
		$totalpages 	= ceil($totalitems/$perpage);
		//adjust the query to take pagination into account
		if(!empty($paged) && !empty($perpage)){
			$offset		= ($paged-1)*$perpage;
			$query		.= ' LIMIT '.(int)$offset.','.(int)$perpage;
		}

		/* -- Register the pagination -- */
		$this->set_pagination_args( 
			array(
				"total_items" => $totalitems,
				"total_pages" => $totalpages,
				"per_page" => $perpage,
			) 
		);
		//The pagination links are automatically built according to those parameters

		/* -- Register the Columns -- */
		$columns 	= $this->get_columns();
		$hidden 	= array();
		$sortable	= $this->get_sortable_columns();
		// $_wp_column_headers[$screen->id]=$columns;
		$this->_column_headers = array($columns, $hidden, $sortable);

		/* -- Fetch the items -- */
		$this->items = $wpdb->get_results($query);
	}
	
	/**
	 * Display the rows of records in the table
	 * @return string, echo the markup of the rows
	 */
	
	// function display_rows() {
// 		//Get the records registered in the prepare_items method
// 		$records = $this->items;
//
// 		//Get the columns registered in the get_columns and get_sortable_columns methods
// 		list( $columns, $hidden ) = $this->get_column_info();
//
// 		//Loop for each record
// 		if(!empty($records)){
//
// 			foreach($records as $rec){
//
// 				//Open the line
// 				echo '< tr id="record_'.$rec->id.'">';
//
// 				foreach ( $columns as $column_name => $column_display_name ) {
//
// 					//Style attributes for each col
// 					$class = "class='$column_name column-$column_name'";
// 					$style = "";
// 					if ( in_array( $column_name, $hidden ) ) $style = ' style="display:none;"';
// 					$attributes = $class . $style;
//
// 					//edit link
// 					// $editlink  = '/wp-admin/link.php?action=edit&link_id='.(int)$rec->link_id;
// 					$editlink  = 'javascript:void(0)';
//
// 					//Display the cell
// 					switch ( $column_name ) {
// 						case "col_link_uID":
// 							echo '< td '.$attributes.'>'.stripslashes($rec->id).'< /td>';
// 						break;
// 						case "col_link_uKey":
// 							echo '< td '.$attributes.'>'.stripslashes($rec->uKey).'< /td>';
// 						break;
// 						case "col_link_uLname":
// 							echo '< td '.$attributes.'>'.stripslashes($rec->keyData).'< /td>';
// 						break;
// 						case "col_link_uEnrolled":
// 							echo '< td '.$attributes.'>'.stripslashes($rec->eiip).'< /td>';
// 						break;
// 						case "col_link_ttd":
// 							echo '< td '.$attributes.'>'.stripslashes($rec->ttd).'< /td>';
// 						break;
// 					}
// 				}
// 			}
// 		}
//
// 		//Close the line
// 		echo'< /tr>';
// 	}
	
	// function display()
// 	{
// 		var_dump($this->items);
// 	}
}
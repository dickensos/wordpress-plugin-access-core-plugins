<div class="wrap">
<h2>Access Core Plugins - Administration</h2>
<br><br>
<?php
	if( !empty($_POST) ):
		$update					= sanitize_text_field($_POST['activate_update']);
		$core_plugin_updates	= array();
		// $staff_member	= sanitize_text_field($_POST['staff_member']) == 'on' ? 1 : 0;
	
	

		if( $update == 'true' ){
			
			foreach( $_POST as $plugin_name => $post_val ):
				if( strrpos($plugin_name, 'check_plugin_') > -1 ){
					
					if( $post_val  === 'on' ){
						$clean = str_replace('check_plugin_', '', $plugin_name);
						$core_plugin_updates[]	= $clean;
					}
				}
			endforeach;
			
			update_option( 'acp_core_plugins', maybe_serialize( $core_plugin_updates), 'no' );
		}
	endif;//close update conditional
	
	$plugins		= get_plugins();
	$core_plugins	= maybe_unserialize( get_option('acp_core_plugins') );
?>
<form action="" method="post" accept-charset="utf-8">
	<table class="form-table">
		<tr valign="top"><th scope="row">Plugin Name:</th>
			<td>Core Plugin?</td>
		</tr>
<?php
	foreach( $plugins as $location => $plugin ):
?>
	<tr valign="top"><th scope="row"><?php echo $plugin['Name']; ?></th>
		<td><input type="checkbox" name="<?php echo 'check_plugin_'.$plugin['Name']; ?>" <?php if( in_array(str_replace(' ','_',$plugin['Name']), $core_plugins) ): ?>checked <?php endif; ?> /></td>
	</tr>
<?php endforeach; ?>	
	<input type="hidden" name="activate_update" value="true" />
	</table>
	<?php submit_button(); ?>
</form>
</div><?php //Close WP wrap ?>
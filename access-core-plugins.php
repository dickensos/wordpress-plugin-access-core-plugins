<?php
/*
 Plugin Name: Access Core Plugins
 Plugin URI: http://www.evanjwarden.com/
 Description: Custom plugin for managing access to "core" app plugins, restricted by capabilities.
 Version: 0.1
 Author: Jamie Warden
 Author URI: http://www.evanjwarden.com
 Text Domain: access-core-plugins
 */

/*  Copyright 2009-2015	Evan J. Warden  (email : jamie@evanjwarden.com)*/

$page 			= '';

register_activation_hook( __FILE__, 'initial_activation');
function initial_activation()
{
	add_option( 'acp_core_plugins', '', '', 'no' );
	
	// gets the admin role
	$role = get_role( 'administrator' );
	// This only works, because it accesses the class instance.
	// would allow the author to edit others' posts for current theme only
	$role->add_cap( 'access_core_plugins' );
}

register_uninstall_hook( __FILE__, 'uninstaller');
function uninstaller()
{
	delete_option( 'acp_core_plugins' );
}

// add_action( 'admin_init', 'register_styles' );
function register_styles()
{
	wp_register_style( 	'user-list-table', plugin_dir_url(__FILE__) . 'assets/css/user-list-table-style.css' );
}

function enqueue_the_styles()
{
	wp_enqueue_style( 'user-list-table' );
}

add_action('admin_menu', 'access_core_plugins_menu');
function access_core_plugins_menu(){
	$page = add_menu_page( 'Access Core Plugins Administration', 'Access Core Plugins', 'access_core_plugins', 'access-core-plugins', 'generate_list_page' );
}

function generate_list_page(){
	require_once('access-core-plugins-dashboard.php');
}

// define the all_plugins callback
add_filter( 'all_plugins', 'filter_core_plugins', 10, 1 );
function filter_core_plugins( $result ) 
{	
	$core_plugins	= maybe_unserialize( get_option('acp_core_plugins') );
	
	if( is_array($core_plugins) && !current_user_can('access_core_plugins') ):
		foreach( $result as $key => $plugin ):
			if( in_array(str_replace(' ', '_', $plugin['Name']), $core_plugins) ){
				unset($result[$key]);
			}
		endforeach;
	endif;
	
	return $result;
};

add_filter('plugin_action_links', 'filter_core_plugin_actions', 10, 4);
function filter_core_plugin_actions($actions, $plugin_file, $plugin_data, $context)
{
	$core_plugins	= maybe_unserialize( get_option('acp_core_plugins') );
	
	if( is_array($core_plugins) && !current_user_can('access_core_plugins') ):
		if( in_array(str_replace(' ', '_', $plugin_data['Name']), $core_plugins) ){
			$actions = array();
		}
	endif;
	
	return $actions;
}

?>
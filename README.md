Usage
=====

Install the plugin in the normal way. This will create a new dashboard menu item called "Access Core Plugins" and a new administrator capability "access_core_plugins". This cap can be assigned to any user role with a role editor. Plugins marked as "Core Plugin" will be hidden from any user that does not have the "access_core_plugins" capability.

About
=====

Version: 0.1

Written by Jamie Warden of [Brand Culture Company](http://www.brandculture.com)
